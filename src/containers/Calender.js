import React, { Component } from 'react';
import {connect} from 'react-redux';
import Month from '../components/Month';
import Week from '../components/Week';
import {incrementDate, decrementDate} from '../actions';
import "../css/CalenderColors.css";

export const MONTHLY_MODE = "MONTHLY_MODE";
export const WEEK_MODE = "WEEK_MODE";

class Calender extends Component{
    constructor(props) {
        super(props);
        this.state = {selectedMode: WEEK_MODE}

        this.incrementDate = this.incrementDate.bind(this);
        this.decrementDate = this.decrementDate.bind(this);

    }
    render(){
        console.log(this.state);
        return (
            <div className="calender">
                <div>
                    <a href="#" onClick={() => this.setState({selectedMode: MONTHLY_MODE})}> Måned </a>
                    <a href="#" onClick={() => this.setState({selectedMode: WEEK_MODE})}> Uke </a>
                </div>
                {this.viewMode()}
            </div>
        )
    }
    viewMode(){
        switch(this.state.selectedMode){
            case MONTHLY_MODE:
                return <Month 
                selectedDate={this.props.selectedDate} 
                currentDate={this.props.currentDate}
                incrementDate={this.incrementDate}
                decrementDate={this.decrementDate}
                />
            case WEEK_MODE:
                return <Week/>
            default:
                return <div>Select mode</div>
        }
    }

    incrementDate() {
        this.props.incrementDate(this.state.selectedMode);
    }
    decrementDate() {
        this.props.decrementDate(this.state.selectedMode);
    }
}

export default connect(({dates}) => dates, {incrementDate, decrementDate})(Calender);