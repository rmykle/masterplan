import {combineReducers} from 'redux';
import dates from './Dates';


export default combineReducers({
    dates
});

