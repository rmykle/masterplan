
export const INCREMENT_DATE = "INCREMENT_DATE";
export const DECREMENT_DATE = "DECREMENT_DATE";

export const incrementDate = (mode) => {
    return {
        type: INCREMENT_DATE,
        payload: mode
    }
}

export const decrementDate = (mode) => {
    return {
        type: DECREMENT_DATE,
        payload: mode
    }
}