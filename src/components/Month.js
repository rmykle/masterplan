import React, { Component } from 'react';
import {connect} from 'react-redux';
import { getWeekFromDate } from '../tools/Dates';
import '../css/Month.css';

class Month extends Component{

    constructor(props) {
        super(props);
        this.keyCount = 100;
        this.getKey = this.getKey.bind(this);
    }

    render() {

        const {selectedDate} = this.props;
        let startDate = new Date(
            selectedDate.getFullYear(), 
            selectedDate.getMonth(), 1);
        const modifier = startDate.getDay() == 0 ? 6 : startDate.getDay() -1;
        startDate.setDate(startDate.getDate() - modifier);
        
        const currentMonth = selectedDate.getMonth();
        const lastDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth()+1, 0);
        const lastModifier  = 7 - lastDate.getDay();
        lastDate.setDate(lastDate.getDate() + lastModifier);
        


        let days = [];
        while(startDate <= lastDate){
            if(startDate.getMonth() != currentMonth) {
                days.push(this.fillerCell(startDate));
            }
            else {
                days.push(this.dayCell(startDate));
            }
            startDate.setDate(startDate.getDate() +1);
        }
        
     

        const weeks= [];
        while(days.length) weeks.push(days.splice(0,7));

        return(
            <div className="monthWrap">
               
               <div className="monthSelector">
                    <a href="#" onClick={()=> this.props.decrementDate()}>
                     {`« ${currentMonth == 0 ? this.monthAsString(11) : this.monthAsString(currentMonth-1)}`}
                    </a>
                    <p>{this.monthAsString(currentMonth)}</p>
                    <a href="#" onClick={()=> this.props.incrementDate()}>
                        {`${currentMonth == 11 ? this.monthAsString(0) : this.monthAsString(currentMonth+1)} »`}
                    </a>
                </div>

                <div className="month" key={this.getKey()}>
                    <div className="daysLevel">
                    {
                        " Mandag Tirsdag Onsdag Torsdag Fredag Lørdag Søndag"
                        .split(" ")
                        .map(day => <div key={day}>{day}</div>)
                    }
                    </div>
                    {
                        weeks.map(week => {
                            const weekNum = getWeekFromDate(week.filter(check => !check.filler)[0].date);
                            return (
                                <div className="week" key={weekNum}>
                                    <div className="weekNum">{`Uke ${weekNum}`}</div>
                                    {week.map(day => {
                                        return (
                                        <div className={day.classes} key={this.getKey()}>
                                            {day.date.getDate()}
                                        </div>
                                        )
                                    })}
                                </div>
                            )


                        })
                    }
                        
                </div>
            </div>
        )        
    }

    monthAsString(month){
        const months = {0: "Januar", 1: "Februar", 2: "Mars", 3: "April", 4: "Mai", 5: "Juni", 
        6: "Juli", 7: "August", 8: "September", 9: "Oktober", 10: "November", 11: "Desember"};
        return months[month];

    }


    dayCell(date){
        
        const {currentDate} = this.props;
        const isToday = currentDate.getDate() == date.getDate() && currentDate.getMonth() == date.getMonth();
        const isWeekend = date.getDay() == 0 || date.getDay() == 6;
        let classes = `${isToday ? "currentDay" : isWeekend ? "weekend" : "day"}`;
        return {date: new Date(date.getFullYear(), date.getMonth(), date.getDate()), classes: classes, filler:false}
    }
    
    fillerCell(date){
        return {filler: true, classes: "fillerDay day", date: new Date(date.getFullYear(), date.getMonth(), date.getDate())}
    }

    getKey() {
        return this.keyCount++;
    }
}

export default connect(null, null)(Month);