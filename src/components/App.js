import React from 'react';
import "../css/style.css";
import bulma from 'bulma';
import Header from './Header';
import Calender from '../containers/Calender';

export default () => {
    return (
        <div>
            <Header/>
            <Calender/>
        </div>
    )
}